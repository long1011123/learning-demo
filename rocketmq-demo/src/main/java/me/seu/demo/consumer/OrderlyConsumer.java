package me.seu.demo.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * orderly 消费者
 *
 * @author liangfeihu
 * @since 2020/4/16 16:30
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "orderly_topic", consumerGroup = "my-consumer_orderly_topic", consumeMode = ConsumeMode.ORDERLY, consumeThreadMax = 4)
public class OrderlyConsumer implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt msgExt) {
        try {
            log.info("[{}] queueId={} received orderly msg: {}", Thread.currentThread().getName(), msgExt.getQueueId(), new String(msgExt.getBody(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
