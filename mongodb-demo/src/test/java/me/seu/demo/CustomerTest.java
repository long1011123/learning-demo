package me.seu.demo;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import lombok.extern.slf4j.Slf4j;
import me.seu.demo.dao.CustomerRepository;
import me.seu.demo.model.Customer;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author liangfeihu
 * @since 2020/4/28 14:47
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoDemoApplication.class)
public class CustomerTest {

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void crudCustomerData() {
        repository.deleteAll();

        // save a couple of customers
        repository.save(new Customer("Alice", "Smith"));
        repository.save(new Customer("Bob", "Smith"));

        // fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();

        // fetch an individual customer
        System.out.println("Customer found with findByFirstName('Alice'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByFirstName("Alice"));

        System.out.println("Customers found with findByLastName('Smith'):");
        System.out.println("--------------------------------");
        for (Customer customer : repository.findByLastName("Smith")) {
            System.out.println(customer);
        }
    }

    @Test
    public void testGetCollection() {

        List<Customer> customerList = new ArrayList<>();
        customerList.add(new Customer("Alice", "Bill"));
        customerList.add(new Customer("Job", "Bill"));
        Collection<Customer> customers = mongoTemplate.insertAll(customerList);
        log.info("save data ={}", customers);

        String collectionName = mongoTemplate.getCollectionName(Customer.class);
        log.info("collectionName = {}", collectionName);
        Set<String> collectionNames = mongoTemplate.getCollectionNames();
        log.info("collectionNames = {}", collectionNames);

        MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
        long countDocuments = collection.countDocuments();
        log.info("countDocuments = {}", countDocuments);

        DistinctIterable<String> filenameList = mongoTemplate.getCollection(collectionName).distinct("firstName", String.class);
        for (String str : filenameList) {
            log.info("filenameList distinct ={}", str);
        }
    }

}
