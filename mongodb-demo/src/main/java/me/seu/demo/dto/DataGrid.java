package me.seu.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * 返回前端页面对象
 *
 * @author liangfeihu
 * @since 2020/4/27 19:08
 */
@Data
@NoArgsConstructor
public class DataGrid<T> {

    /**
     * 当前页面号
     */
    private int current;
    /**
     * 每页行数
     */
    private int rowCount;
    /**
     * 总行数
     */
    private int total;
    /**
     *
     */
    private List<T> rows;

}
