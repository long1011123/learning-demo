package me.seu.demo.dao;

import me.seu.demo.model.Picture;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * MongoDB Dao
 *
 * @author liangfeihu
 * @since 2020/4/27 19:00
 */
@Repository
public interface PictureRepository extends MongoRepository<Picture, String> {

    /**
     * 根据id获取PO
	 *
     * @param id
     * @return
     */
    @Override
    Optional<Picture> findById(String id);

    /**
     * 获取所有数据，带分页排序
	 *
	 * @param pageable
	 * @return
     */
    @Override
    Page<Picture> findAll(Pageable pageable);


    /**
     * 根据文件名过滤，带分页排序
     *
     * @param filename
     * @param pageable
     * @return
     */
    Page<Picture> findByFilenameContaining(String filename, Pageable pageable);

    /**
     * 根据文件名过滤的结果总数
     *
     * @param filename
     * @return
     */
    List<Picture> findByFilenameContaining(String filename);

}
