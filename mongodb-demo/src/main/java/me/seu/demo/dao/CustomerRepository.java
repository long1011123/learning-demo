package me.seu.demo.dao;

import me.seu.demo.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liangfeihu
 * @since 2020/4/28 14:44
 */
@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

    /**
     * 根据firstName查询
     *
     * @param firstName
     * @return
     */
    public Customer findByFirstName(String firstName);

    /**
     * 根据lastName查询
     *
     * @param lastName
     * @return
     */
    public List<Customer> findByLastName(String lastName);

}
