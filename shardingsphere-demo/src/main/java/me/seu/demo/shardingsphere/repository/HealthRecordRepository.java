package me.seu.demo.shardingsphere.repository;

import me.seu.demo.shardingsphere.entity.HealthRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 健康记录表 Dao
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:45
 */
@Mapper
public interface HealthRecordRepository extends BaseRepository<HealthRecord, Long> {

}
