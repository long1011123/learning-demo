package me.seu.demo.shardingsphere.service;

import me.seu.demo.shardingsphere.entity.EncryptUser;

import java.sql.SQLException;
import java.util.List;

/**
 * 加密用户 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:01
 */
public interface EncryptUserService {

    public void processEncryptUsers() throws SQLException;

    public List<EncryptUser> getEncryptUsers() throws SQLException;

}
