package me.seu.demo.shardingsphere.service.impl;

import me.seu.demo.shardingsphere.entity.HealthLevel;
import me.seu.demo.shardingsphere.repository.HealthLevelRepository;
import me.seu.demo.shardingsphere.service.HealthLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * 健康级别 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:06
 */
@Service
public class HealthLevelServiceImpl implements HealthLevelService {

    @Autowired
    private HealthLevelRepository healthLevelRepository;

    @Override
    public void processLevels() throws SQLException {
        healthLevelRepository.addEntity(new HealthLevel(11L, "level11"));
        healthLevelRepository.addEntity(new HealthLevel(12L, "level12"));
        healthLevelRepository.addEntity(new HealthLevel(13L, "level13"));
        healthLevelRepository.addEntity(new HealthLevel(14L, "level14"));
        healthLevelRepository.addEntity(new HealthLevel(15L, "level15"));
    }

}
