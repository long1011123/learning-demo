package me.seu.demo.shardingsphere;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import me.seu.demo.shardingsphere.service.HealthLevelService;
import me.seu.demo.shardingsphere.service.HealthRecordService;
import me.seu.demo.shardingsphere.service.UserService;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Shardingsphere 启动类
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:23
 */
@ComponentScan("me.seu.demo.shardingsphere")
@MapperScan(basePackages = "me.seu.demo.shardingsphere.repository")
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class})
public class ShardingsphereDemoApplication {

    public static void main(String[] args) throws SQLException, IOException {
        //SpringApplication.run(ShardingsphereDemoApplication.class, args);

        try (ConfigurableApplicationContext applicationContext = SpringApplication.run(ShardingsphereDemoApplication.class, args)) {

            UserService userService = applicationContext.getBean(UserService.class);
            userService.processUsers();
            userService.getUsers();

            HealthLevelService healthLevelService = applicationContext.getBean(HealthLevelService.class);
            healthLevelService.processLevels();

            HealthRecordService healthRecordService = applicationContext.getBean(HealthRecordService.class);
            healthRecordService.processHealthRecords();

//        	HintService hintService = applicationContext.getBean(HintService.class);
//        	hintService.processWithHintValueForShardingDatabases();
//        	hintService.processWithHintValueForShardingDatabasesAndTables();
//        	hintService.processWithHintValueMaster();
        }

    } // end main

}
