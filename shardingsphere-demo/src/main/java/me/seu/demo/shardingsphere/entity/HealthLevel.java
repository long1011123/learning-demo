package me.seu.demo.shardingsphere.entity;

/**
 * 健康级别表
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:30
 */
public class HealthLevel {

    Long levelId;
    String levelName;

    public HealthLevel(Long levelId, String levelName) {
        this.levelId = levelId;
        this.levelName = levelName;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

}
