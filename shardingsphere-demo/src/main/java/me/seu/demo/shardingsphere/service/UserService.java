package me.seu.demo.shardingsphere.service;

import me.seu.demo.shardingsphere.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 * 用户 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:03
 */
public interface UserService {

    public void processUsers() throws SQLException;

    public List<User> getUsers() throws SQLException;

}
