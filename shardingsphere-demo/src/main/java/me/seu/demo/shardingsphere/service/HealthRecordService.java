package me.seu.demo.shardingsphere.service;

import java.sql.SQLException;

/**
 * 健康记录 Service
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午10:02
 */
public interface HealthRecordService {

    public void processHealthRecords() throws SQLException;

}
