package me.seu.demo.shardingsphere.repository;

import me.seu.demo.shardingsphere.entity.EncryptUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 加密用户表Dao
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:43
 */
@Mapper
public interface EncryptUserRepository extends BaseRepository<EncryptUser, Long> {

}
