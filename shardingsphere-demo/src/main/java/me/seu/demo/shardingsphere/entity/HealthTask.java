package me.seu.demo.shardingsphere.entity;

/**
 * 健康任务表
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/5/6 上午9:30
 */
public class HealthTask {

    Long taskId;
    Long userId;
    Long recordId;
    String taskName;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

}
