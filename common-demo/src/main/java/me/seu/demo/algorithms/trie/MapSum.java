package me.seu.demo.algorithms.trie;

import java.util.TreeMap;


/**
 * Trie树
 *
 * @author liangfeihu
 * @since 2018/12/5 14:54
 */
public class MapSum {

    /**
     * 元素节点Class类
     */
    private class Node {

        public int value;
        public TreeMap<Character, Node> next;

        public Node(int value) {
            this.value = value;
            next = new TreeMap<>();
        }

        public Node() {
            this(0);
        }
    }

    /**
     * 字典树 头节点
     */
    private Node root;

    /**
     * Initialize your data structure here.
     */
    public MapSum() {
        root = new Node();
    }

    /**
     * 向Trie中添加一个新的元素
     *
     * @param key
     * @param val
     */
    public void insert(String key, int val) {
        Node cur = root;
        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if (cur.next.get(c) == null) {
                cur.next.put(c, new Node());
            }
            cur = cur.next.get(c);
        }
        cur.value = val;
    }

    /**
     * 求前缀节点值得累加和
     *
     * @param prefix
     * @return
     */
    public int sum(String prefix) {
        Node cur = root;
        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            if (cur.next.get(c) == null) {
                return 0;
            }
            cur = cur.next.get(c);
        }

        return sum(cur);
    }

    private int sum(Node node) {
        int res = node.value;
        for (char c : node.next.keySet()) {
            res += sum(node.next.get(c));
        }
        return res;
    }

}
