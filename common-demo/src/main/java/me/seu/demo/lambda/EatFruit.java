package me.seu.demo.lambda;

/**
 * 函数式接口
 *
 * @author liangfeihu
 * @since 2020/3/4 15:56
 */
@FunctionalInterface
public interface EatFruit {
    /**
     * 吃水果
     */
    void eat();
}

