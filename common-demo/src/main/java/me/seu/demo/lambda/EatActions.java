package me.seu.demo.lambda;

import java.util.List;

/**
 * 吃集合
 *
 * @author liangfeihu
 * @since 2020/3/4 16:25
 */
public class EatActions {
    private List<EatFruit> eatList;

    public EatActions(List<EatFruit> eatList) {
        this.eatList = eatList;
    }

    public void eatAction() {
        System.out.println("----------Eat Actions start-------------");
        for (EatFruit eatFruit : eatList) {
            eatFruit.eat();
        }
        System.out.println("----------Eat Actions end -------------");
    }
}
