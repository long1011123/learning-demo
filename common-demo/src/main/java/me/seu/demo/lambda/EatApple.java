package me.seu.demo.lambda;

/**
 * 吃苹果
 *
 * @author liangfeihu
 * @since 2020/3/4 15:59
 */
public class EatApple implements EatFruit {
    private final String fruitName = "Apple";

    /**
     * 吃苹果
     */
    @Override
    public void eat() {
        System.out.println("People like to eat " + fruitName + " !");
    }
}
