package me.seu.demo.classic;

/**
 * @author liangfeihu
 * @since 2020/1/22 11:06
 */
public class Singleton2 {
    /*volatile*/static Singleton2 instance;

    static Singleton2 getInstance() {
        if (instance == null) {
            synchronized (Singleton2.class) {
                if (instance == null) {
                    instance = new Singleton2();
                }
            }
        }

        return instance;
    }
}
