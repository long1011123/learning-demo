package me.seu.demo.service.bean;

import org.springframework.stereotype.Component;

/**
 * Service One
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/27 下午5:06
 */
@Component
public class OneService extends BaseResource {
    private static final String API = "One";

    public OneService() {
        super(API);
    }

    public OneService(String name) {
        super(name);
    }

}
