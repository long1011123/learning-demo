package me.seu.demo.list;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liangfeihu
 * @since 2020/1/8 10:44
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> strs = new ArrayList<>();
        strs.add("hello");
        for (String str : strs) {
            System.out.println(str);
        }
    }
}
