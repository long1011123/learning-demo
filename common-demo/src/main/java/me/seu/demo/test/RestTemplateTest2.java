package me.seu.demo.test;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author liangfeihu
 * @since 2020/6/17 17:49
 */
public class RestTemplateTest2 {

    public static void main(String[] args) {
        String url = "https://hcp-prod.leapstack.cn/gw/num/login";

        RestTemplate restTemplate = new RestTemplate();

        String requestBody = "{\"password\":\"Aa123456\",\"username\":\"yiankefu\"}";

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody, headers);

        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        System.out.println(stringResponseEntity.getBody());
    }

}
