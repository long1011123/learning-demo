package me.seu.demo.test.time;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.MessageFormat;

/**
 * 年月日 时分秒
 *
 * @author liangfeihu
 * @number 53669
 * @since 2021/4/6 下午2:22
 */
public class DateTimeTest {
    public static final String PREMISSION_STRING = "perms[{0}]";

    public static void main(String[] args) {
        DateTime dateTime = new DateTime();
        String timeStr = dateTime.toString("yyyy-MM-dd HH:mm:ss");
        System.out.println(timeStr);
        int year = dateTime.getYear();
        int month = dateTime.getMonthOfYear();
        int day = dateTime.getDayOfMonth();

        int hour = dateTime.getHourOfDay();
        int minute = dateTime.getMinuteOfHour();
        int second = dateTime.getSecondOfMinute();

        System.out.println("年" + year + " 月" + month + " 日" + day);
        System.out.println("时" + hour + " 分" + minute + " 秒" + second);

        byte[] dt = new byte[] {0x0F, 0x0C, 0x1D, 0x00, 0x00, 0x15};
        System.out.println("年" + dt[0] + " 月" + dt[1] + " 日" + dt[2]);
        System.out.println("时" + dt[3] + " 分" + dt[4] + " 秒" + dt[5]);

        System.out.println(1183 / 100d);


        DecimalFormat formater = new DecimalFormat("000000");
        String format = formater.format(123);
        System.out.println(format);

        String hello = MessageFormat.format(PREMISSION_STRING, "hello");
        System.out.println(hello);
    }

}
