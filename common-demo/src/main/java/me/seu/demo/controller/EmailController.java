package me.seu.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.MailSenderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 发邮件接口
 *
 * @author liangfeihu
 * @since 2021/1/25 下午5:00
 */
@Slf4j
@RestController
@RequestMapping("/email")
public class EmailController {

    @Resource
    MailSenderService mailSenderService;

    @GetMapping("/text")
    public ResponseEntity text() {
        mailSenderService.sendMail();
        log.info("[EmailController] send text mail OK");
        return ResponseEntity.ok("send text success");
    }

    @GetMapping("/attach")
    public ResponseEntity textAttach() throws Exception{
        mailSenderService.sendMailAttach();
        log.info("[EmailController] send text attach mail OK");
        return ResponseEntity.ok("send text attach success");
    }

    @GetMapping("/attach/plus")
    public ResponseEntity textAttachPlus() throws Exception{
        mailSenderService.sendMailAttachPlus();
        log.info("[EmailController] send text attach plus mail OK");
        return ResponseEntity.ok("send text attach plus success");
    }

}
