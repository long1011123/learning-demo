package me.seu.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.common.CommonResult;
import me.seu.demo.exception.RestException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试全局异常及返回状态码
 *
 * @author liangfeihu
 * @since 2020/3/7 12:54
 */
@Slf4j
@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/custom")
    public CommonResult customException() throws RestException{
        if (Boolean.TRUE) {
            throw new RestException("Test custom exception success");
        }
        return CommonResult.success("Test custom exception success");
    }

    @GetMapping("/system")
    public CommonResult systemException() throws RestException{
        int price = 20 / 0;
        log.info("20 / 0 = {}", price);
        return CommonResult.success("Test system exception success");
    }

}
