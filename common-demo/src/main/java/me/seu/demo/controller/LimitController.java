package me.seu.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.seu.demo.service.AccessLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author liangfeihu
 * @since 2020/4/16 14:51
 */
@Slf4j
@RestController
@RequestMapping("/limit")
public class LimitController {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private AccessLimitService accessLimitService;

    @GetMapping("/access")
    public String access() {
        //尝试获取令牌
        if (accessLimitService.tryAcquire()) {
            //模拟业务执行500毫秒
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "access success [" + sdf.format(new Date()) + "]";
        } else {
            return "access limited [" + sdf.format(new Date()) + "]";
        }
    }

    @GetMapping("/str")
    public String limitAccess() {
        log.info("[limitAccess] limit str = {}", accessLimitService.limitStr);
        log.info("[limitAccess] limit str get method = {}", accessLimitService.getLimitStr());
        return accessLimitService.limitStr;
    }

}
