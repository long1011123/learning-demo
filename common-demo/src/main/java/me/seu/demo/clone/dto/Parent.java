package me.seu.demo.clone.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author liangfeihu
 * @since 2020/1/16 14:38
 */
@Data
@ToString
public class Parent {

    private String name;

    private Integer age;

}
