package me.seu.demo.clone.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author liangfeihu
 * @since 2020/1/16 14:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Son extends Parent {
    /**
     * 属性名与父类相同
     */
    private String name;

    private String hobby;

}
