package me.seu.demo.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * guava cache
 *
 * @author liangfeihu
 * @since 2021/1/8 下午2:31
 */
public class LocalCache {

    private static Cache<String, Long> idCache = CacheBuilder.newBuilder().expireAfterWrite(24, TimeUnit.HOURS).maximumSize(1000L).build();

    public static void main(String[] args) {
        String dateStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
        System.out.println(nextId(dateStr));
        System.out.println(nextId(dateStr));
        System.out.println(nextId(dateStr));
        System.out.println(nextId(dateStr));
    }

    public static String nextId(String date) {
        Long result = idCache.getIfPresent(date);
        if (Objects.isNull(result)) {
            idCache.put(date, 1L);
            result = 1L;
        } else {
            idCache.put(date, ++result);
        }
        return String.format("%04d", result);
    }

}
